# Automate CSV 
# pip install pandas
import pandas
# Read CSV File
data = pandas.read_csv("test.csv")
# Read CSV Specific Column
data = pandas.read_csv("test.csv", usecols=["col1", "col2"])
# Read CSV Specific Rows
data = pandas.read_csv("test.csv", nrows=5)
# Read CSV Specific Rows and Columns
data = pandas.read_csv("test.csv", usecols=["col1", "col2"], nrows=5)
# Read CSV File and Convert to JSON
data = pandas.read_csv("test.csv").to_json(orient="records")
# Write CSV File
data = {"col1": ["a", "b", "c"], "col2": [1, 2, 3]}
data = pandas.DataFrame(data)
data.to_csv("test.csv", index=False)
# Append Column to CSV File
data = pandas.read_csv("test.csv")
data["col3"] = ["x", "y", "z"]
data.to_csv("test.csv", index=False)
# Append Row to CSV File
data = pandas.read_csv("test.csv")
data = data.append({"col1": "d", "col2": 4}, ignore_index=True)
data.to_csv("test.csv", index=False)
# Drop Column from CSV File
data = pandas.read_csv("test.csv")
data = data.drop(columns=["col3"])
data.to_csv("test.csv", index=False)
# Drop Row from CSV File
data = pandas.read_csv("test.csv")
data = data.drop([2])
data.to_csv("test.csv", index=False)
import requests
import json

webhook_url = 'https://webhook.site/4ca0a733-92f9-4654-af08-423f3e072537'

data = { 'name': 'Matt M', 
        'channel': 'https://info.site' }

r = requests.post(webhook_url, data=json.dumps(data), headers={'Content-Type': 'application/json'})